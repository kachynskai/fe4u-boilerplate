import {additionalUsers, randomUserMock} from "./FE4U-Lab2-mock.js";



let changedArr=[];
let favoriteTechers=[];
let usersArr;
let agesCounter=[0,0,0,0];
function assimilates(){
    let course=["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess",
        "Biology","Chemistry","Law", "Art","Medicine", "Statistics"];
  _.map(usersArr,function (users){
    // let rand_fav=Math.random()<0.5;
    let colors=["#dface7","#1f75cb","cadetblue","darkorchid", "cornflowerblue","greenyellow","tomato","darkslategray"]
    let rand_course=Math.floor(Math.random() * course.length);
    let rand_color=Math.floor(Math.random() * colors.length);
    let new_user={
      "gender":users.gender,
      "title":users.name.title,
      "full_name":users.name.first+" "+users.name.last,
      "id":users.id.name+users.id.value,
      "course":course[rand_course],
      "city":users.location.city,
      "state":users.location.state,
      "country":users.location.country,
      "postcode":users.location.postcode,
      "coordinates":users.location.coordinates,
      "timezone":users.location.timezone,
      "email":users.email,
      "favorite":false,
      "b_date":users.dob.date,
      "age":users.dob.age,
      "phone":users.phone,
      "picture_Large":users.picture.large,
      "picture_thumbnail":users.picture.thumbnail,
      "bg_color":colors[rand_color],
      "note": "Lorem Ipsum dolor sit amet, consectetur adipisicing elit."
    }
      fillArrCountAges(new_user);
    if(new_user.favorite===true){
        favoriteTechers.push(new_user);
    }
    changedArr.push(new_user);
  });
}
export {changedArr};
function validates(user){
  if((typeof user.full_name === "string" && user.full_name.charAt(0) === user.full_name.charAt(0).toUpperCase()) &&
      (typeof user.gender === "string") &&
      (typeof user.note === "string" && user.note.charAt(0) === user.note.charAt(0).toUpperCase()) &&
      (typeof user.state === "string" && user.state.charAt(0) === user.state.charAt(0).toUpperCase()) &&
      (typeof user.city === "string" && user.city.charAt(0) === user.city.charAt(0).toUpperCase()) &&
      (typeof user.country === "string" && user.country.charAt(0) === user.country.charAt(0).toUpperCase()) &&
      ((/^\d+$/.test(user.age))&&typeof user.age==="number") &&
      (/^[0-9()+ -]+$/.test(user.phone)) &&
      (user.email.includes("@"))) return true;
 return false;
}
function makesCorrect(user){

  //make correct string and uppercase
  if(user.full_name&& !_.isString(user.full_name )) user.full_name = String(user.full_name);
  if(user.full_name)user.full_name=user.full_name.charAt(0).toUpperCase()+user.full_name.slice(1);
  if(user.gender&& !_.isString(user.gender)) user.gender = String(user.gender);
  if(user.note && !_.isString(user.note ))user.note = String(user.note);
  if(user.note)user.note=user.note.charAt(0).toUpperCase()+user.note.slice(1);
  if(user.state && !_.isString(user.state)) user.state = String(user.state);
  if(user.state)user.state=user.state.charAt(0).toUpperCase()+user.state.slice(1);
  if(user.city && !_.isString(user.city )) user.city = String(user.city);
  if(user.city)user.city=user.city.charAt(0).toUpperCase()+user.city.slice(1);
  if(user.country && !_.isString(user.country)) user.country = String(user.country);
  if(user.country)user.country=user.country.charAt(0).toUpperCase()+user.country.slice(1);
  //make correct age and other
  if (user.age && isNaN(user.age)) user.age ="undefined";
  if (user.age && !_.isNumber(user.age)) user.age =_.toInteger(user.age);
  if(user.phone&&(!/^[0-9()+ -]+$/.test(user.phone))) user.phone="000-000-000";
  if(user.email&&(!user.email.includes("@")))user.email="something@example.com";
  return user;
}
function filters(filter_parameter){
    let ages = filter_parameter.age.split("-");
    let min_age = parseInt(ages[0]);
    let max_age = parseInt(ages[1]);

    const filteredUsers = _.filter(changedArr, function (user) {
        return (
            (user.age && user.age >= min_age && user.age <= max_age) &&
            (!filter_parameter.country || user.country === filter_parameter.country) &&
            (!filter_parameter.gender || user.gender === filter_parameter.gender) &&
            (!filter_parameter.photo || user.hasOwnProperty("picture_Large")) &&
            (!filter_parameter.favorite || (user.hasOwnProperty("favorite") && user.favorite === true))
        );
    });

    return filteredUsers;
}
function sorts(array_for_sort, parameter,bool){
  let sorted=[...array_for_sort];
  let enable_for_sort = sorted.filter(user => user.hasOwnProperty(parameter));
  let not_enable_for_sort = sorted.filter(user => !user.hasOwnProperty(parameter));

    if(parameter==="b_date"){
      enable_for_sort.forEach(user =>{
        if (user.hasOwnProperty("b_date")){
          user["b_date"]= new Date(user["b_date"]);
        }
        if (user.hasOwnProperty("b_day")){
          user["b_date"]= new Date(user["b_day"]);
        }
      })
    }
    enable_for_sort.sort((a,b)=>{
      let user_a=a[parameter];
      let user_b=b[parameter];
      if(user_a>user_b) return -1;
      else if(user_a<user_b)return 1;
      return 0;
    })

  if(!bool){
    enable_for_sort.reverse();
  }
  sorted=[...enable_for_sort,...not_enable_for_sort];
  return sorted;
}
function lookingFor(parameter_value){
    return _.find(changedArr, function (user) {
        if (_.isNumber(parameter_value)) {
            return user.age === parameter_value;
        } else {
            return (user.full_name === parameter_value && user.hasOwnProperty("full_name")) ||
                (user.note === parameter_value && user.hasOwnProperty("note"));
        }
    });
}
function calculates(parameter, parameter_value){
  let amount=[];
  if(parameter === 'age') amount = changedArr.filter((users) => users.age >= parameter_value);
  else  amount = changedArr.filter((users) => users[parameter] === parameter_value);
  let result=(amount.length / changedArr.length)*100;
  result=parseFloat(result.toFixed(3));
  return result;
}
//TASK4
let currentPage=1;
let teachersList=document.querySelector(".teachers-list");
let curArrForSort=[...changedArr.slice(currentPage*10-10,currentPage*10)];
getUsers();
function getUsers(){
    fetch("https://randomuser.me/api/?results=50")
        .then(r =>r.json() )
        .then(data=>{
            let users=data.results;
            usersArr=[...users];
            assimilates();
            for(let users of changedArr){
                if(!validates(users)){
                    makesCorrect(users);
                }
                teachersList.appendChild(teachersDrawing(users));
            }
            curArrForSort=[...changedArr.slice(currentPage*10-10,currentPage*10)];
            createPieChart();
            // drawTable(curArrForSort);
            // drawButTable();
            renderFavorites();
        })
        .catch(error=>{
            console.error("Exception: ",error);
        })

}
//START TASK3
function teachersDrawing(user){

        let teachItem=document.createElement("div");
        teachItem.className="teacher-item";
        let nameArr =user.full_name.split(" ");
        if(user.favorite===true){
            teachItem.className+=" favor-teach";
            let starImg=document.createElement("img");
            starImg.className="star";
            starImg.src="images/star.png";
            starImg.alt="star";
            teachItem.appendChild(starImg);
        }
        let imageTeach=document.createElement("div");
        imageTeach.className="image-teach";
        if(user.hasOwnProperty("picture_Large")){
            let imgT=document.createElement("img");
            imgT.src=user.picture_Large;
            imgT.alt="Image of teacher";
            imageTeach.appendChild(imgT);
        }else{
            imageTeach.className+=" without-img";
            let initials=document.createElement("label");
            initials.textContent=nameArr[0].charAt(0)+"."+nameArr[1].charAt(0)+".";
            imageTeach.appendChild(initials);
        }
        let fName=document.createElement("span");
        fName.className="name";
        fName.textContent=nameArr[0];
        let sName=document.createElement("span");
        sName.className="name";
        sName.textContent=nameArr[1];
        let spec= document.createElement("span");
        spec.className="speciality";
        spec.textContent=user.course;
        let country=document.createElement("span");
        country.className="country";
        country.textContent=user.country;
        teachItem.appendChild(imageTeach);
        teachItem.appendChild(fName);
        teachItem.appendChild(sName);
        teachItem.appendChild(spec);
        teachItem.appendChild(country);
       return teachItem;
}
let isPopupOpen=false;
let curTeacher;
    teachersList.addEventListener("click",(event)=>{
        if(isPopupOpen){return;}
        let target=event.target;
        let targetClosest=target.closest(".teacher-item");

            let name=targetClosest.querySelectorAll(".name");
            let text= name[0].textContent+" "+name[1].textContent;
            curTeacher=targetClosest;
            addInfoPop(lookingFor(text));
    })


function addInfoPop(user){
    let currentDate=dayjs();
    let birthDate=dayjs(user.b_date);
    let nextBirthDay=dayjs(birthDate).set('year',currentDate.year());
    if(nextBirthDay.isBefore(currentDate))nextBirthDay.add(1,'year');
    let daysToBirthDay=nextBirthDay.diff(currentDate,"day");
    if(daysToBirthDay<=0)daysToBirthDay=365+daysToBirthDay;
    console.log("next"+daysToBirthDay);
    let popup=document.getElementById("pop-inf");
    popup.style.display="block";
    popup.parentElement.style.display="block";
    isPopupOpen=true;
    let head=document.createElement("section");
    head.className="header";
    let nameHeader=document.createElement("span");
    nameHeader.textContent="Teacher Info";
    let but=document.createElement("button");
    but.textContent="x";
    but.addEventListener("click",()=>{
        popup.style.display="none";
        popup.parentElement.style.display="none";
        popup.innerHTML="";
        isPopupOpen=false;
        if (curTeacher){
            teachersList.replaceChild(teachersDrawing(user),curTeacher);
        }
        })
    head.appendChild(nameHeader);
    head.appendChild(but);
    let nameArr =user.full_name.split(" ");
    let information= document.createElement("section");
    information.className="information";
    let imgDiv=document.createElement("div");
    imgDiv.className="image-info";
    if(user.hasOwnProperty("picture_Large")){
        let image=document.createElement("img");
        image.src=user.picture_Large;
        image.alt="Image of teacher";
        imgDiv.appendChild(image);
    }else{
        imgDiv.className+=" without-img";
        let initials=document.createElement("label");
        initials.textContent=nameArr[0].charAt(0)+"."+nameArr[1].charAt(0)+".";
        imgDiv.appendChild(initials);
    }
    information.appendChild(imgDiv);
    let mainDiv= document.createElement("div");
    mainDiv.className="main-info";
    let nameInfo=document.createElement("span");
    nameInfo.className="name-info";
    nameInfo.textContent=user.full_name;
    let specInfo=document.createElement("span");
    specInfo.className="speciality-info";
    specInfo.textContent=user.course;
    let locInfo=document.createElement("span");
    locInfo.className="loc";
    locInfo.textContent=user.city+", "+user.country;
    let ageInfo=document.createElement("span");
    ageInfo.className="loc";
    ageInfo.textContent=user.age+", "+user.gender;
    let emailInfo=document.createElement("a");
    emailInfo.href="";
    emailInfo.textContent=user.email;
    let numberInfo=document.createElement("span");
    numberInfo.className="number";
    numberInfo.textContent=user.phone;
    let dob=document.createElement("span")
    dob.className="loc";
    dob.textContent=daysToBirthDay+" days left until the next birthday";
    mainDiv.appendChild(nameInfo);
    mainDiv.appendChild(specInfo);
    mainDiv.appendChild(locInfo);
    mainDiv.appendChild(ageInfo);
    mainDiv.appendChild(emailInfo);
    mainDiv.appendChild(numberInfo);
    mainDiv.appendChild(dob);
    information.appendChild(mainDiv);
    let star=document.createElement("div");
    star.className="star-empty";
    let imgStar=document.createElement("img");
    imgStar.className="empty";
    if(user.favorite===false){
        imgStar.src="images/emptyStar.png";
    }else{
        imgStar.src="images/star.png";
    }
    imgStar.addEventListener("click",()=>{
        user.favorite = !user.favorite;
        if(user.favorite===false){
            imgStar.src="images/emptyStar.png";
            favoriteTechers=favoriteTechers.filter(favUser=> favUser!==user);
            renderFavorites();
        }else{
            imgStar.src="images/star.png";
            favoriteTechers.push(user);
            renderFavorites();
        }
    });

    star.appendChild(imgStar);
    information.appendChild(star);
    let textDiv=document.createElement("div");
    textDiv.className="text-info";
    let paragraph=document.createElement("p");
    paragraph.textContent=user.note;
    textDiv.appendChild(paragraph);
    information.appendChild(textDiv);
    let detail= document.createElement("details");
    detail.className="map";
    let summary=document.createElement("summary");
    summary.textContent="toggle map";
    let mapDiv=document.createElement("div");
    mapDiv.id="map";
    detail.appendChild(summary);
    detail.appendChild(mapDiv)
    information.appendChild(detail);
    popup.appendChild(head);
    popup.appendChild(information);
    console.log(user);


    let map = L.map('map').setView([user.coordinates.latitude, user.coordinates.longitude], 15);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    L.marker([user.coordinates.latitude, user.coordinates.longitude]).addTo(map)
        .openPopup();

}
let headers=document.getElementById("thead");
    let pieChart;
function createPieChart(){
    let container= document.getElementById("piechart");
    if (pieChart) {
        pieChart.destroy();
    }
    pieChart=new Chart(container,{
        type: "pie",
        data:{
            labels:["18-31", "32-41", "42-51","52-95"],
            datasets: [{
                label:"age gradation",
                data: agesCounter
            }]
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Statistics of teachers by age',
                    font: {
                        size: 18
                    }
                }

            }
        }
    })
}
function fillArrCountAges(user){
    _.map(agesCounter, function (count, index) {
        if (
            (index === 0 && user.age >= 18 && user.age <= 31) ||
            (index === 1 && user.age >= 32 && user.age <= 41) ||
            (index === 2 && user.age >= 42 && user.age <= 51) ||
            (index === 3 && user.age >= 52 && user.age <= 95)
        ) {
            agesCounter[index] += 1;
        }
    });
}
function drawButTable (){
    console.log(changedArr.length);
    let dimension=Math.ceil(changedArr.length/10);
    let butDiv=document.getElementsByClassName("stats")[0];
    let html="";
    for(let i=1; i<=dimension+1;i++){
        if(i===1){
            html+='<button id="'+i+'" class="but-stats-current st">'+i+'</button>';
        }else if(i===dimension){
            html+='<button id="0" class="others st">...</button>';
        }else if(i===dimension+1){
            html+='<button id="'+dimension+'" class="but-stats st">Last</button>';
        }
        else{html+='<button id="'+i+'" class="but-stats st">'+i+'</button>';}
    }
    butDiv.innerHTML=html;
    let buttons=document.getElementsByClassName("st");
    for(let but of buttons ){
        but.addEventListener("click", ()=>{
            let curBut=document.getElementsByClassName("but-stats-current")[0];
            if(parseInt(curBut.id)===0){
                curBut.className="others st";
            }
            else{
                curBut.className="but-stats st";
            }
            but.className="but-stats-current st";
                currentPage=parseInt(but.id);
            if(currentPage!==0){
                curArrForSort=[...changedArr.slice(currentPage*10-10,currentPage*10)];
            }else {
                curArrForSort=[...changedArr];
            }
                drawTable(curArrForSort);
        })
    }
}
function drawTable(array){
    let tableBody=document.getElementsByTagName("tbody");
    let html="";
    for(let i=0; i<array.length; i++){
        html+='<tr>' +
            '<td class="left-name">'+array[i].full_name+'</td>' +
            '<td>'+array[i].course+'</td>' +
            '<td>'+array[i].age+'</td>' +
            '<td>'+array[i].gender+'</td>' +
            '<td>'+array[i].country+'</td>'+
            '</tr>'

    }
    tableBody[0].innerHTML=html;
}
document.getElementsByClassName("but-search")[0].addEventListener("click",()=>{
    let info=document.getElementsByClassName("inp-for-search")[0].value;
    if (/^[0-9]+$/.test(info)){
        info=parseInt(info);
    }
    if(info!==""){
        let user=lookingFor(info);
        if(user!==undefined){
            let fullName=user.full_name;
            let users=document.querySelectorAll(".teacher-item");
            for(let item of users){
                let nameArr=item.querySelectorAll(".name");
                let correct=nameArr[0].textContent+" "+nameArr[1].textContent;
                if(correct===fullName){
                    curTeacher=item;
                }
            }
            addInfoPop(user);
        }
        document.getElementsByClassName("inp-for-search")[0].value="";
    }
})
document.getElementById("ageSelect").addEventListener("change",renderByFilter);
document.getElementById("region").addEventListener("change",renderByFilter);
document.getElementById("sex").addEventListener("change",renderByFilter);
document.getElementById("photo").addEventListener("change",renderByFilter);
document.getElementById("favorites").addEventListener("change",renderByFilter);

function renderByFilter(){
    let filters_param={
        "country":document.getElementById("region").value,
        "age": document.getElementById("ageSelect").value,
        "gender": document.getElementById("sex").value,
        "photo":document.getElementById("photo").checked,
        "favorite":document.getElementById("favorites").checked
    }
    let filterTeacher=filters(filters_param);
    teachersList.innerHTML="";
    for (let teacher of filterTeacher){
        teachersList.appendChild(teachersDrawing(teacher));
    }
}
for (let but of document.getElementsByClassName("but-add")){
    but.addEventListener("click",()=>{
        document.getElementById("popup-back").style.display="block";
        document.getElementById("popup-add").style.display="block";

    })
}
document.getElementById("close-popup-add").addEventListener("click",()=>{
    document.getElementById("popup-add").style.display="none";
    document.getElementById("popup-back").style.display="none";

})
let dateInput = document.getElementById("email_us");
document.getElementById("form-add-teacher").addEventListener("submit",(evnt)=>{
    evnt.preventDefault();
    let gender;
    let title;
    let curDate=new Date().getFullYear();
    let date=document.getElementById("date_birth").value;
    if(document.getElementById("g1").checked){
        gender="male";
        title="Mr";
    }else{
        gender="female";
        title="Mrs";
    }
    let newUser={
        "gender":gender,
        "title":title,
        "full_name":document.getElementById("us_name").value,
        "id":document.getElementById("us_name").value.split(" ").join("")+date,
        "coordinates":{
            "latitude":50.46444693229159,
            "longitude": 30.519415686134504
        },
        "course":document.getElementById("spec").value,
        "city":document.getElementById("cities").value,
        "country":document.getElementById("countr").value,
        "email":document.getElementById("email_us").value,
        "favorite":false,
        "b_date":date,
        "age":curDate-date.split("-")[0],
        "phone":document.getElementById("phone_num").value,
        "bg_color":document.getElementById("colour").value,
        "note": document.getElementById("comment").value
    };
    if(!validates(newUser))newUser=makesCorrect(newUser);
//  ex5 of TASK 4
    fetch('http://localhost:3000/posts', {
        method: 'POST',
        body: JSON.stringify(newUser),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (response.ok) {
                alert("User was successfully added into JSON");
            } else {
                alert("Error: users was not added into JSON");
            }
        })
        .catch(error => {
            console.error("Exception", error);
        });

    changedArr.push(newUser);
    teachersList.appendChild(teachersDrawing(newUser));
    fillArrCountAges(newUser);
    createPieChart();
    // drawTable(curArrForSort);
    // drawButTable();
    document.getElementById("popup-add").style.display="none";
    document.getElementById("popup-back").style.display="none";
    const form = document.getElementById("form-add-teacher");
    const formElements = form.elements;
    for (const element of formElements) {
        if (element.tagName!=="label") {
            element.value = '';
        }
    }
})
function renderFavorites(){
    let favDiv=document.getElementById("slides");

    let html="";
    for(let i=0; i<favoriteTechers.length;i++){
        let nameArr =favoriteTechers[i].full_name.split(" ");
        html+='<div class="teacher-item slide">';
        if(favoriteTechers[i].hasOwnProperty("picture_Large")){
            html+='<div class="image-teach"><img src="'+favoriteTechers[i].picture_Large+'" alt="Image of teacher"/></div>';
        }else{
            html+='<div class="image-teach without-img"><label>'+nameArr[0].charAt(0)+"."+nameArr[1].charAt(0)+"."+'</label></div>';
        }
        html+='<span class="name">'+nameArr[0]+'</span>' +
            '<span class="name">'+nameArr[1]+'</span>' +
            '<span class="country">'+favoriteTechers[i].country+'</span>' +
            '</div>';
    }
    favDiv.innerHTML=html;
}
let position=0;
let num=5;
let allWidth=document.getElementById("slides-track").offsetWidth;
let width=allWidth/num;
let slides=document.getElementById("slides");
document.getElementById("left").addEventListener("click",()=>{
    if(favoriteTechers.length>num){
        position+=width;
        position=Math.min(position,0);
        slides.style.transform='translate('+position+'px)';
    }
})
document.getElementById("right").addEventListener("click",()=>{
    if(favoriteTechers.length>num) {
        position -= width;
        position = Math.max(position, -width * (favoriteTechers.length - num));
        slides.style.transform = 'translate(' + position + 'px)';
    }
})